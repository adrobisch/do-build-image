FROM openjdk:19-jdk-alpine

RUN apk add --no-cache curl zip unzip docker bash git nodejs npm rustup gcc musl-dev \
    && curl -sL https://github.com/digitalocean/doctl/releases/download/v1.100.0/doctl-1.100.0-linux-amd64.tar.gz | tar -xzv \
    && mv doctl /usr/local/bin \
    && curl -LO "https://dl.k8s.io/release/v1.28.3/bin/linux/amd64/kubectl" \
    && chmod +x kubectl \ 
    && mv kubectl /usr/local/bin \
    && curl -sL https://get.helm.sh/helm-v3.13.1-linux-amd64.tar.gz | tar -xzv \
    && mv linux-amd64/helm /usr/local/bin \
    && rm -r linux-amd64 \
    && curl -sL https://github.com/sbt/sbt/releases/download/v1.9.7/sbt-1.9.7.zip --output sbt.zip \
    && unzip sbt.zip \
    && rm sbt.zip \
    && mv sbt /opt/sbt \
    && ln -s /opt/sbt/bin/sbt /usr/local/bin/sbt \
    && rm -rf /var/cache/apk/*

RUN addgroup -S build \
    && adduser -S build -G build \
    && adduser build docker

WORKDIR /home/build

USER build

RUN sbt exit \
    && echo 'export PATH="$HOME/.cargo/bin:$PATH"' >> /home/build/.bashrc \
    && echo 1 | rustup-init
